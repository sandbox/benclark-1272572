<?php

/**
 * Simple translatable custom text box.
 */
class boxes_i18n extends boxes_simple {

  /**
   * Implementation of boxes_content::options_defaults().
   */
  public function options_defaults() {
    $defaults = parent::options_defaults();
    return $defaults + array(
      'i18nboxes' => array(
        'language' => I18NBOXES_LOCALIZE,
      ),
    );
  }

  /**
   * Implementation of boxes_content::options_form().
   */
  public function options_form() {
    $form = parent::options_form();

    $form['i18nboxes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Multilingual settings'),
      '#collapsible' => TRUE,
      '#weight' => 0,
      '#tree' => TRUE, // To avoid conflict with i18nblocks.
    );

    $options = array('' => t('All languages'));
    $options[I18NBOXES_LOCALIZE] = t('All languages (Translatable)');
    $options += locale_language_list('name');

    $form['i18nboxes']['language'] = array(
      '#type' => 'radios',
      '#title' => t('Language'),
      '#default_value' => $this->options['i18nboxes']['language'],
      '#options' => $options,
    );

    return $form;
  }

  /**
   * Save a box.
   */
  public function save() {
    parent::save();
    // Make the box strings available to be translated.
    $this->locale_refresh();
  }

  /**
   * Refresh the translatable strings.
   */
  public function locale_refresh() {
    $title = '';
    $content = '';
    if ($this->options['i18nboxes']['language'] == I18NBOXES_LOCALIZE) {
      $content = $this->options['body'];
      $title = $this->title;
    }
    // Update or delete the i18nstrings record for the body.
    i18nstrings_update("boxes:$this->plugin_key:$this->delta:body", $content, $this->options['format']);
    // Update or delete the title, too.
    i18nstrings_update("boxes:$this->plugin_key:$this->delta:title", $title);
  }

  /**
   * Implementation of boxes_content::render().
   */
  public function render() {
    global $language;
    // Get the filtered rendered data from boxes_simple.
    $block = parent::render();
    // Handle any multilingual settings.
    if (!empty($this->options['i18nboxes']['language'])) {
      if ($this->options['i18nboxes']['language'] == I18NBOXES_LOCALIZE) {
        // This box is available to translate.
        $block['content'] = i18nstrings_text("boxes:$this->plugin_key:$this->delta:body", $block['content']);
        // Also translate the title.
        $block['subject'] = i18nstrings("boxes:$this->plugin_key:$this->delta:title", $block['subject']);
        $block['title'] = $block['subject'];
      }
      elseif ($this->options['i18nboxes']['language'] != $language->language) {
        // This box does not share the language of the user, so do not display.
        $block = NULL;
      }
    }
    return $block;
  }

}
